package com.example.mycompass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button CompassBtn;
    private Button AccBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CompassBtn = findViewById(R.id.compassButton);
        AccBtn = findViewById(R.id.acceleratorButton);

        CompassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                moveToCompass();
            }

        });

        AccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                moveToAccelerator();
            }

        });

    }

    public void moveToCompass() {
        Intent intent = new Intent(this, CompassActivity.class);
        startActivity(intent);
    }

    private void moveToAccelerator() {
        Intent intent = new Intent(this, AcceleratorActivity.class);
        startActivity(intent);
    }

}
